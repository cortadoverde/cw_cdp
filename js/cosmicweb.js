(function($, root, window){

    /**
     * Cosmicweb script personalizado
     */

    'use strict';

    // Dom Ready
    $(function(){
        $('a[title="modal"]').each(function(){
            var $el = $(this);

            $el.removeAttr('title');
            $el.addClass('fancybox.iframe')
            $el.fancybox({
                maxWidth    : 600,
                maxHeight   : 450,
                fitToView   : false,
                width       : '70%',
                height      : '70%',
                autoSize    : false,
                closeClick  : false,
                openEffect  : 'none',
                closeEffect : 'none'
            
            })
        })


        $('a[href="#Contacto"]').fancybox();
    });

})(jQuery, this, window)