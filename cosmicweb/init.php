<?php
    /**
        Customizacion de CosmicWeb para Cdp Logistica

        ns => cw_[type]_[action]

    **/

    add_filter( 'body_class', 'cw_filter_body_class' );

    function cw_filter_body_class( $arrClases )
    {
        array_unshift($arrClases, 'cosmicweb-page');
        return $arrClases;
    }

    add_shortcode( 'cw_post', 'cw_post' );
    function cw_post( $attrs )
    {

        $_def = array(
            'style' => 'cosmicweb',
            'id'    => false
        );

        $attrs = array_merge($_def, $attrs);

        extract($attrs);

        if( ! $id )
            return '';

        ob_start();

            query_posts('p = '.$id );

            get_template_part( 'includes/view/cosmic/post', $style);

            wp_reset_query();
            $output = ob_get_clean();

        return $output;
    }

    add_shortcode( 'cw_menu', 'cw_sc_menu' );
    function cw_sc_menu( $attrs ) {
      $_def = array(
          'menu' => 'main'
      );

      $attrs = array_merge($_def, $attrs);

      extract($attrs);

      if( !isset( $id ) )
        $menu_id = 'short_menu_'.uniqid();
      else
        $menu_id = $id;



      $defaults = array(
				'theme_location'  => '',
				'menu'            => $menu,
				'container'       => 'div',
				'container_class' => '',
				'container_id'    => '',
				'menu_class'      => 'cosmic_menu',
				'menu_id'         => '',
				'echo'            => false,
				'fallback_cb'     => 'wp_page_menu',
				'before'          => '',
				'after'           => '',
				'link_before'     => '',
				'link_after'      => '',
				'items_wrap'      => '<ul id="'.$menu_id.'" class="%2$s">%3$s</ul>',
				'depth'           => 0,
				'walker'          => ''
			);




      // $filter_priority = has_filter( 'wp_nav_menu_args', codeless_custom_menu::$id_filter.'change_menu_arguments' );
      // if ( false !== $filter_priority ) {
      //  remove_filter( 'wp_nav_menu_args', codeless_custom_menu::$id_filter.'change_menu_arguments', $filter_priority );
      // }

      $html = wp_nav_menu( $defaults );

      // if ( false !== $filter_priority ) {
      //  add_filter( 'wp_nav_menu_args', codeless_custom_menu::$id_filter.'change_menu_arguments', $filter_priority );
      // }

      return $html;
    }
