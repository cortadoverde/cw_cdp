<?php

//while (have_posts()) :

        the_post();

        $post_id    = get_the_ID();

        $title      = get_the_title();

        $subtitle   = get_post_meta($post_id, 'post_subtitle', true);

        $has_sub    = trim($subtitle) != '' && $subtitle != 'vacio';

        $content    = get_the_content();

        $content    = str_replace(']]>', ']]&gt;', apply_filters('the_content', $content ));
        ?>

        <article id="post-<?php echo the_ID(); ?>" <?php echo post_class('cosmicweb normal ' . $seccion); ?>>
            <div class="heading <?php echo $has_sub ? 'with subtitle' : '' ?>">
                <h1>
                    <a href="<?php echo get_permalink() ?>"><?php echo esc_html(get_the_title()) ?></a>
                </h1>
                <?php if( $has_sub ) : ?>
                    <h2><?php echo $subtitle ?></h2>
                <?php endif; ?>
            </div>

            <div class="content">
                <?php the_content(); ?>
            </div>
        </article>

        <?php
//endwhile;
